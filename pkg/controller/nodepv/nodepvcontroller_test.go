/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package nodepv

/*
import (
	"testing"

	"k8s.io/kubernetes/pkg/api"
	clientset "k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset"
	"k8s.io/kubernetes/pkg/client/clientset_generated/internalclientset/fake"
	"k8s.io/kubernetes/pkg/controller"
	"k8s.io/kubernetes/pkg/controller/informers"
	"k8s.io/kubernetes/pkg/util/wait"
	"k8s.io/kubernetes/pkg/api/unversioned"
)

func NewNodePVControllerFromClient(
	kubeClient clientset.Interface) (*NodePVController, error) {

	factory := informers.NewSharedInformerFactory(kubeClient, controller.NoResyncPeriodFunc())

	nc, err := NewNodePVController(factory.Nodes(), factory.PersistentVolumes(), kubeClient, wait.NeverStop)
	if err != nil {
		return nil, err
	}

	return nc, nil
}
func TestMonitorNodeStatusEvictPods(t *testing.T) {
	table := []struct {
		fakeNodePVHandler  *FakeNodePVHandler
		expectedDeletePods []*api.Pod
		expectedUpdatePVs  []*api.PersistentVolume
		description        string
	}{
		// Node created recently, with no status (happens only at cluster startup).
		{
			fakeNodePVHandler: &FakeNodePVHandler{
				ExistingPod []*api.Pod {
					newPod("default", "pod1", "node1"),
				}
				Clientset: fake.NewSimpleClientset(&api.NodeList{
					Items: []api.Node{
						*newNode("node1",api.ConditionTrue,	unversioned.Date(2015, 1, 1, 12, 0, 0, 0, time.UTC)),
					},
				},
				),
			},
			expectedDeletePods: []*api.Pod{},
			expectedUpdatePVs: []*api.PersistentVolume{},
			description:         "Node created recently, with no status.",
		},
	}
	for _, item := range table {
		nodeController, _ := NewNodePVControllerFromClient( item.fakeNodePVHandler )

		if err := nodeController.monitorNodeStatus(); err != nil {
			t.Errorf("unexpected error: %v", err)
		}
		if item.timeToPass > 0 {
			nodeController.now = func() unversioned.Time { return unversioned.Time{Time: fakeNow.Add(item.timeToPass)} }
			item.fakeNodeHandler.Existing[0].Status = item.newNodeStatus
			item.fakeNodeHandler.Existing[1].Status = item.secondNodeNewStatus
		}
		if err := nodeController.monitorNodeStatus(); err != nil {
			t.Errorf("unexpected error: %v", err)
		}
		zones := getZones(item.fakeNodeHandler)
		for _, zone := range zones {
			nodeController.zonePodEvictor[zone].Try(func(value TimedValue) (bool, time.Duration) {
				nodeUid, _ := value.UID.(string)
				deletePods(item.fakeNodeHandler, nodeController.recorder, value.Value, nodeUid, nodeController.daemonSetStore)
				return true, 0
			})
		}

		podEvicted := false
		for _, action := range item.fakeNodeHandler.Actions() {
			if action.GetVerb() == "delete" && action.GetResource().Resource == "pods" {
				podEvicted = true
			}
		}

		if item.expectedEvictPods != podEvicted {
			t.Errorf("expected pod eviction: %+v, got %+v for %+v", item.expectedEvictPods,
				podEvicted, item.description)
		}
	}
}
*/
